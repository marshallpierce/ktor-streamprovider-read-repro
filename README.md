Reproduction of a ktor bug.

Reading individual bytes from multipart item streams just reads the first byte forever instead of traversing across the input.
