package org.mpierce.ktor.streamprovider.repro

import io.ktor.application.call
import io.ktor.http.ContentDisposition
import io.ktor.http.ContentType
import io.ktor.http.HttpHeaders
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.readAllParts
import io.ktor.http.content.streamProvider
import io.ktor.http.headersOf
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.io.streams.asInput
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.InputStream

class StreamProviderTest {
    private val bytes = byteArrayOf(100, 101, 102, 103, 104, 105)

    @Test
    internal fun testWithReadAll() {
        runTestWithBytes {
            it.readAllBytes()
        }
    }

    /**
     * This test will fail; it gets the first byte 6 times
     */
    @Test
    internal fun testWithReadSingle() {
        runTestWithBytes {
            byteArrayOf(
                    it.read().toByte(),
                    it.read().toByte(),
                    it.read().toByte(),
                    it.read().toByte(),
                    it.read().toByte(),
                    it.read().toByte()
            )
        }
    }

    private fun runTestWithBytes(byteExtractor: (InputStream) -> ByteArray) {
        withTestApplication({
            routing {
                post("/upload") {
                    val part = call.receiveMultipart().readPart() as PartData.FileItem

                    val bytes = withContext(Dispatchers.IO) {
                        part.streamProvider().use { stream ->
                            byteExtractor(stream)
                        }.toList()
                    }

                    call.respond(HttpStatusCode.OK, "bytes $bytes")
                }
            }
        }) {
            with(handleRequest(HttpMethod.Post, "/upload") {
                val boundary = "***bbb***"

                addHeader(HttpHeaders.ContentType,
                        ContentType.MultiPart.FormData.withParameter("boundary", boundary).toString())
                setBody(boundary, listOf(
                        PartData.FileItem({ bytes.inputStream().asInput() }, {},
                                headersOf(
                                        HttpHeaders.ContentDisposition,
                                        ContentDisposition.File
                                                .withParameter(ContentDisposition.Parameters.Name, "file")
                                                .withParameter(ContentDisposition.Parameters.FileName, "file")

                                                .toString()
                                ))
                ))
            }) {
                assertEquals(HttpStatusCode.OK, response.status())
                assertEquals("bytes ${bytes.asList()}", response.content!!)
            }
        }
    }
}
